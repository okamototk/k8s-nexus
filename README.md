# Nexius manifest for Kubernetes

Nexus manifest for Kubernetes. When you create container registry, please use 5000/TCP.

## install
    $ kubectl create ns nexus
    $ kubectl apply -f nexus.yaml
    $ kubectl apply -f nexus-lb.yaml

## Get admin password

To get admin password for first login:

    $ kubectl cp nexus-0:nexus-data/admin.password admin.password  -nnexus
    $ cat admin.password

## Accessa

Check Loadbalancer IP address:

    $ kubectl get svc -nnexus
    NAME        TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)                       AGE
    nexus-svc   LoadBalancer   10.0.189.96   20.44.197.111   80:30964/TCP,5000:30705/TCP   5m35s

Access http://10.0.189.96/ by browser. Use 80 port to publish nexus UI.


